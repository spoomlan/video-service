package org.nudt.video.service

import org.nudt.common.model.ShortVideo
import org.nudt.common.repository.ShortVideoRepository
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service

@Service
class ShortVideoService(private val shortVideoRepository: ShortVideoRepository) {
    
    fun shortVideoListRandom(limits: Int): List<ShortVideo> {
        return shortVideoRepository.getShortVideosOrderByRand(PageRequest.ofSize(limits))
    }
}