package org.nudt.video.service

import jakarta.persistence.criteria.Predicate
import org.nudt.common.exception.BadRequestException
import org.nudt.common.model.PlayLog
import org.nudt.common.model.Video
import org.nudt.common.model.vo.VideoVO
import org.nudt.common.repository.PlayLogRepository
import org.nudt.common.repository.VideoRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
class VideoService(private val videoRepository: VideoRepository, private val playLogRepository: PlayLogRepository) {
    fun getVideoListByType(type: Int, pageable: Pageable): Page<VideoVO> {
        return videoRepository.getByTypeIdOrderByVodTime(type, pageable).map { VideoVO.of(it) }
    }
    
    fun getVideoListRecommendByType(type: Int, pageable: Pageable): List<VideoVO> {
        return videoRepository.getVideosOrderByRand(type, pageable).map { VideoVO.of(it) }
    }
    
    @Transactional
    fun getVideoById(id: Int): Video {
        //todo 访问后添加一条playLog
        val video = videoRepository.findById(id).orElseThrow { throw BadRequestException("视频数据未找到") }
        val playLog = PlayLog(videoId = video.vodId, userId = 14, appid = "439fbfb3958d06c3")
        playLogRepository.save(playLog)
        return video
    }
    
    fun getBannerVideoList(banner: Int): List<VideoVO> {
        return videoRepository.getByVodLevel(banner).map { VideoVO.of(it) }
    }
    
    fun searchVideoList(keyword: String): List<VideoVO> {
        return videoRepository.getByVodNameContainsIgnoreCase(keyword).map { VideoVO.of(it) }
    }
    
    fun filterVideoList(type: Int, area: String?, year: Int?, minYear: Int, pageable: Pageable): Page<VideoVO> {
        val spec = Specification<Video> { root, query, cBuilder ->
            val predicates: MutableList<Predicate> = ArrayList()
            if (type > 0) {
                val typeCondition = cBuilder.equal(root.get<Int>("type"), type)
                predicates.add(typeCondition)
            }
            if (area.equals("中国") || area.equals("美国") || area.equals("韩国") || area.equals("日本")) {
                val areaCondition = cBuilder.like(root.get("area"), "%$area%")
                predicates.add(areaCondition)
            }
            
            if (area == "其他地区") {
                val areaConditionChina = cBuilder.notLike(root.get("area"), "%中国%")
                val areaConditionUSA = cBuilder.notLike(root.get("area"), "%美国%")
                val areaConditionKorea = cBuilder.notLike(root.get("area"), "%韩国%")
                val areaConditionJapan = cBuilder.notLike(root.get("area"), "%日本%")
                predicates.add(areaConditionChina)
                predicates.add(areaConditionUSA)
                predicates.add(areaConditionKorea)
                predicates.add(areaConditionJapan)
            }
            if (year != null) {
                if (year > minYear) {
                    val yearCondition = cBuilder.equal(root.get<Int>("year"), year)
                    predicates.add(yearCondition)
                } else {
                    val yearCondition = cBuilder.lessThan(root.get("year"), minYear + 1)
                    predicates.add(yearCondition)
                }
            }
            return@Specification cBuilder.and(*predicates.toTypedArray())
            
        }
        return videoRepository.findAll(spec, pageable).map { VideoVO.of(it) }
    }
    
    
}