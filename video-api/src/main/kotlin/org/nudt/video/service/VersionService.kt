package org.nudt.video.service

import org.nudt.common.model.Version
import org.nudt.common.repository.VersionRepository
import org.springframework.stereotype.Service

@Service
class VersionService(private val versionRepository: VersionRepository) {
    
    fun latestVersion(): Version {
        return versionRepository.findFirstByIdNotNullOrderByVersionDesc()
    }
}