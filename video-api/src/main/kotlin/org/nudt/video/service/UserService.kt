package org.nudt.video.service

import org.nudt.common.exception.BadRequestException
import org.nudt.common.model.User
import org.nudt.common.model.dto.UserDto
import org.nudt.common.utils.FileUtil
import org.nudt.common.repository.UserRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.util.*

@Service
class UserService(
        private val userRepository: UserRepository,
        @Value("\${file.avatar-path}") private val avatarPath: String,
        @Value("\${file.avatar-max-size}") private val avatarMaxSize: Long,
) {
    
    @Transactional
    fun appLogin(userDto: UserDto): User {
        val user = userRepository.findByAppid(userDto.appid)
        return if (user.isEmpty) {
            val newUser = User(
                username = UUID.randomUUID().toString(),
                nickname = userDto.nickname ?: "昵称",
                appid = userDto.appid,
                avatar = "",
                gender = userDto.gender ?: 0,
                phone = userDto.phone ?: "",
                email = userDto.email ?: "",
                appVersion = userDto.appVersion ?: "",
                osVersion = userDto.osVersion ?: 0,
                deviceName = userDto.deviceName ?: ""
            )
            userRepository.save(newUser)
            newUser
        } else {
            user.get()
        }
    }
    
    @Transactional(rollbackFor = [Exception::class])
    fun updateAvatar(appid: String, multipartFile: MultipartFile): User {
        val user = userRepository.findByAppid(appid).orElseThrow()
        FileUtil.checkSize(avatarMaxSize, multipartFile.size)
        val imageTypes = "jpg png jpeg webp"
        val fileExt = FileUtil.getExtensionName(multipartFile.originalFilename)
        if (fileExt != null && !imageTypes.contains(fileExt)) {
            throw BadRequestException("文件格式错误！, 仅支持 $imageTypes 格式")
        }
        val file = FileUtil.upload(multipartFile, avatarPath)
        if (file == null) {
            throw BadRequestException("上传文件出错")
        } else {
            user.avatar = "http://localhost/upload/avatar/${file.name}"
            userRepository.save(user)
            return user
        }
    }
    
    @Transactional(rollbackFor = [Exception::class])
    fun updateUserInfo(appid: String, nickname: String?, gender: Int?, phone: String?, email: String?): User {
        val user = userRepository.findByAppid(appid).orElseThrow()
        nickname?.apply {
            user.nickname = nickname
        }
        gender?.let {
            user.gender = gender
        }
        phone?.apply {
            user.phone = phone
        }
        email?.apply {
            user.email = email
        }
        userRepository.save(user)
        return user
    }
}