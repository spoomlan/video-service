package org.nudt.video.controller

import org.nudt.common.model.ResponseData
import org.nudt.common.model.Video
import org.nudt.common.model.vo.VideoVO
import org.nudt.video.service.VideoService
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/videos")
class VideoController(private val videoService: VideoService) {
    
    /**
     * 根据分类获取视频列表
     * @param type 视频大类
     */
    @GetMapping("")
    fun videoListSortByTime(@RequestParam type: Int, @RequestParam page: Int, @RequestParam size: Int): ResponseData<Page<VideoVO>> {
        return ResponseData.success(videoService.getVideoListByType(type, PageRequest.of(page - 1, size)))
    }
    
    /**
     * 根据筛选条件获取视频列表
     * @param type 视频大类
     * @param area 视频地区
     * @param year 视频年份
     */
    @GetMapping("/{type}/filter")
    fun filterVideoList(
        @PathVariable type: Int,
        @RequestParam(required = false) area: String?,
        @RequestParam(required = false) year: Int?,
        @RequestParam minYear: Int,
        @RequestParam page: Int,
        @RequestParam size: Int
    ): ResponseData<Page<VideoVO>> {
        return ResponseData.success(videoService.filterVideoList(type, area, year, minYear, PageRequest.of(page - 1, size)))
    }
    
    /**
     * 搜索视频
     * @param keyword 关键词
     */
    @GetMapping("/search")
    fun searchVideo(@RequestParam keyword: String): ResponseData<List<VideoVO>> {
        return ResponseData.success(videoService.searchVideoList(keyword))
    }
    
    /**
     * 根据视频id获取视频详情
     * @param id 视频id
     */
    @GetMapping("/{id}")
    fun videoDetail(@PathVariable id: Int): ResponseData<Video> {
        return ResponseData.success(videoService.getVideoById(id))
    }
    
    /**
     * 根据分类获取推荐视频列表
     * @param type 视频大类
     */
    @GetMapping("/{type}/recommend")
    fun videoListRecommendByType(@PathVariable type: Int, @RequestParam size: Int): ResponseData<List<VideoVO>> {
        return ResponseData.success(videoService.getVideoListRecommendByType(type, PageRequest.of(0, size)))
    }
    
    /**
     * 发布指定视频的错误信息
     * @param id 视频id
     */
    @PostMapping("/{id}/report")
    fun reportVideoError(@PathVariable id: Int) {
    
    }
    
    /**
     * 获取指定推荐栏视频列表
     * @param banner 推荐栏id
     */
    @GetMapping("/banner/{banner}")
    fun bannerVideoList(@PathVariable banner: Int): ResponseData<List<VideoVO>> {
        return ResponseData.success(videoService.getBannerVideoList(banner))
    }
    
}