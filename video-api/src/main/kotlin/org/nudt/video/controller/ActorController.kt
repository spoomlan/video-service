package org.nudt.video.controller

import org.nudt.video.service.ActorService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/actors")
class ActorController(private val actorService: ActorService) {

}