package org.nudt.video.controller

import org.nudt.common.model.ResponseData
import org.nudt.common.model.ShortVideo
import org.nudt.video.service.ShortVideoService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/shortVideos")
class ShortVideoController(private val shortVideoService: ShortVideoService) {
    
    /**
     * 随机获取短视频
     * @param size 每页多少数据
     */
    @GetMapping("/random")
    fun videoListRandom(@RequestParam size: Int): ResponseData<List<ShortVideo>> {
        return ResponseData.success(shortVideoService.shortVideoListRandom(size))
    }
}