package org.nudt.video.controller

import org.nudt.common.model.ResponseData
import org.nudt.common.model.Version
import org.nudt.video.service.VersionService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/versions")
class VersionController(private val versionService: VersionService) {
    
    /**
     * 获取最新app版本信息
     */
    @GetMapping("/latest")
    fun checkUpdate(): ResponseData<Version> {
        return ResponseData.success(versionService.latestVersion())
    }
}