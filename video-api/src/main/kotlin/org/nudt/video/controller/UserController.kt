package org.nudt.video.controller

import org.nudt.common.model.ResponseData
import org.nudt.common.model.User
import org.nudt.common.model.dto.UserDto
import org.nudt.video.service.UserService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/v1/user")
class UserController(private val userService: UserService) {
    
    /**
     * app登录
     */
    @PostMapping("/login")
    fun appLogin(@RequestBody userDto: UserDto): ResponseData<User> {
        
        return ResponseData.success(userService.appLogin(userDto))
    }
    
    /**
     * 修改app用户头像
     * @param avatar 头像文件
     */
    @PutMapping("/avatar")
    fun updateAvatar(@AuthenticationPrincipal appid: String, @RequestParam avatar: MultipartFile): ResponseData<User> {
        return ResponseData.success(userService.updateAvatar(appid, avatar))
    }
    
    /**
     * 修改app用户信息
     * @param nickname 昵称
     * @param gender 性别
     * @param phone 电话
     * @param email 邮箱
     */
    @PutMapping("")
    fun updateInformation(
        @AuthenticationPrincipal appid: String,
        @RequestParam(required = false) nickname: String?,
        @RequestParam(required = false) gender: Int?,
        @RequestParam(required = false) phone: String?,
        @RequestParam(required = false) email: String?
    ): ResponseData<User> {
        return ResponseData.success(userService.updateUserInfo(appid, nickname, gender, phone, email))
    }
}