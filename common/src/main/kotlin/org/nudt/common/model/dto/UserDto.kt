package org.nudt.common.model.dto

data class UserDto(
    var appid: String,
    var nickname: String? = null,
    var avatar: String? = null,
    var gender: Int? = null,
    var phone: String? = null,
    var email: String? = null,
    var appVersion: String? = null,
    var osVersion: Int? = null,
    var deviceName: String? = null,
)
