package org.nudt.common.model

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import jakarta.persistence.*
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.time.Instant

@Entity
@Table(name = "mac_vod", schema = "video", catalog = "")
data class Video(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id
        @Column(name = "vod_id", nullable = false)
        var vodId: Int? = 0,

        @Basic
        @Column(name = "type_id", nullable = false)
        var typeId: Int = 0,

        @Basic
        @Column(name = "vod_name", nullable = false, length = 255)
        var vodName: String? = null,

        @Basic
        @Column(name = "vod_class", nullable = false, length = 255)
        var vodClass: String? = null,

        @Basic
        @Column(name = "vod_pic", nullable = false, length = 1024)
        var vodPic: String? = null,

        @Basic
        @Column(name = "vod_pic_thumb", nullable = false, length = 1024)
        var vodPicThumb: String? = null,

        @Basic
        @Column(name = "vod_actor", nullable = false, length = 255)
        var vodActor: String? = null,

        @Basic
        @Column(name = "vod_director", nullable = false, length = 255)
        var vodDirector: String? = null,

        @Basic
        @Column(name = "vod_blurb", nullable = false, length = 255)
        var vodBlurb: String? = null,

        @Basic
        @Column(name = "vod_content", nullable = false, length = -1)
        var vodContent: String? = null,

        @Basic
        @Column(name = "vod_remarks", nullable = false, length = 100)
        var vodRemarks: String? = null,

        @Basic
        @Column(name = "vod_year", nullable = false, length = 10)
        var vodYear: String? = null,

        @Basic
        @Column(name = "vod_area", nullable = false, length = 20)
        var vodArea: String? = null,

        @Basic
        @Column(name = "vod_score", nullable = false, precision = 1)
        var vodScore: BigDecimal? = null,

        @Basic
        @Column(name = "vod_level", nullable = false)
        var vodLevel: Byte = 0,

        @Basic
        @Column(name = "vod_play_url", nullable = false, length = -1)
        var vodPlayUrl: String? = null,

        @Basic
        @Column(name = "vod_time", nullable = false)
        @JsonDeserialize(using = TimestampDeserializer::class)
        var vodTime: Long = 0,

        @Basic
        @Column(name = "vod_time_add", nullable = false)
        var vodTimeAdd: Long = 0,

        @Basic
        @Column(name = "type_id_1", nullable = false)
        var typeId1: Short = 0,

        @Basic
        @Column(name = "group_id", nullable = false)
        var groupId: Short = 0,

        @Basic
        @Column(name = "vod_sub", nullable = false, length = 255)
        var vodSub: String? = null,

        @Basic
        @Column(name = "vod_en", nullable = false, length = 255)
        var vodEn: String? = null,

        @Basic
        @Column(name = "vod_status", nullable = false)
        var vodStatus: Byte = 1,

        @Basic
        @Column(name = "vod_letter", nullable = false, length = 1)
        var vodLetter: String? = null,

        @Basic
        @Column(name = "vod_color", nullable = false, length = 6)
        var vodColor: String? = "A",

        @Basic
        @Column(name = "vod_tag", nullable = false, length = 100)
        var vodTag: String? = null,

        @Basic
        @Column(name = "vod_pic_slide", nullable = false, length = 1024)
        var vodPicSlide: String? = null,

        @Basic
        @Column(name = "vod_pic_screenshot", nullable = true, length = -1)
        var vodPicScreenshot: String? = null,


        @Basic
        @Column(name = "vod_writer", nullable = false, length = 100)
        var vodWriter: String? = null,

        @Basic
        @Column(name = "vod_behind", nullable = false, length = 100)
        var vodBehind: String? = null,

        @Basic
        @Column(name = "vod_pubdate", nullable = false, length = 100)
        var vodPubdate: String? = null,

        @Basic
        @Column(name = "vod_total", nullable = false)
        var vodTotal: Int? = 0,

        @Basic
        @Column(name = "vod_serial", nullable = false, length = 20)
        var vodSerial: String? = "0",

        @Basic
        @Column(name = "vod_tv", nullable = false, length = 30)
        var vodTv: String? = null,

        @Basic
        @Column(name = "vod_weekday", nullable = false, length = 30)
        var vodWeekday: String? = null,


        @Basic
        @Column(name = "vod_lang", nullable = false, length = 10)
        var vodLang: String? = "国语",

        @Basic
        @Column(name = "vod_version", nullable = false, length = 30)
        var vodVersion: String? = null,

        @Basic
        @Column(name = "vod_state", nullable = false, length = 30)
        var vodState: String? = null,

        @Basic
        @Column(name = "vod_author", nullable = false, length = 60)
        var vodAuthor: String? = null,

        @Basic
        @Column(name = "vod_jumpurl", nullable = false, length = 150)
        var vodJumpurl: String? = null,

        @Basic
        @Column(name = "vod_tpl", nullable = false, length = 30)
        var vodTpl: String? = null,

        @Basic
        @Column(name = "vod_tpl_play", nullable = false, length = 30)
        var vodTplPlay: String? = null,

        @Basic
        @Column(name = "vod_tpl_down", nullable = false, length = 30)
        var vodTplDown: String? = null,

        @Basic
        @Column(name = "vod_isend", nullable = false)
        var vodIsend: Byte = 1,

        @Basic
        @Column(name = "vod_lock", nullable = false)
        var vodLock: Byte = 0,


        @Basic
        @Column(name = "vod_copyright", nullable = false)
        var vodCopyright: Byte = 0,

        @Basic
        @Column(name = "vod_points", nullable = false)
        var vodPoints: Short = 0,

        @Basic
        @Column(name = "vod_points_play", nullable = false)
        var vodPointsPlay: Short = 0,

        @Basic
        @Column(name = "vod_points_down", nullable = false)
        var vodPointsDown: Short = 0,

        @Basic
        @Column(name = "vod_hits", nullable = false)
        var vodHits: Any? = null,

        @Basic
        @Column(name = "vod_hits_day", nullable = false)
        var vodHitsDay: Int? = 0,

        @Basic
        @Column(name = "vod_hits_week", nullable = false)
        var vodHitsWeek: Int? = 0,

        @Basic
        @Column(name = "vod_hits_month", nullable = false)
        var vodHitsMonth: Int? = 0,

        @Basic
        @Column(name = "vod_duration", nullable = false, length = 10)
        var vodDuration: String? = null,

        @Basic
        @Column(name = "vod_up", nullable = false)
        var vodUp: Int? = 0,

        @Basic
        @Column(name = "vod_down", nullable = false)
        var vodDown: Int? = 0,

        @Basic
        @Column(name = "vod_score_all", nullable = false)
        var vodScoreAll: Int? = 0,

        @Basic
        @Column(name = "vod_score_num", nullable = false)
        var vodScoreNum: Int? = 0,

        @Basic
        @Column(name = "vod_time_hits", nullable = false)
        var vodTimeHits: Int = 0,

        @Basic
        @Column(name = "vod_time_make", nullable = false)
        var vodTimeMake: Int = 0,

        @Basic
        @Column(name = "vod_trysee", nullable = false)
        var vodTrysee: Short = 0,

        @Basic
        @Column(name = "vod_douban_id", nullable = false)
        var vodDoubanId: Int = 0,

        @Basic
        @Column(name = "vod_douban_score", nullable = false, precision = 1)
        var vodDoubanScore: BigDecimal? = null,

        @Basic
        @Column(name = "vod_reurl", nullable = false, length = 255)
        var vodReurl: String? = null,

        @Basic
        @Column(name = "vod_rel_vod", nullable = false, length = 255)
        var vodRelVod: String? = null,

        @Basic
        @Column(name = "vod_rel_art", nullable = false, length = 255)
        var vodRelArt: String? = null,

        @Basic
        @Column(name = "vod_pwd", nullable = false, length = 10)
        var vodPwd: String? = null,

        @Basic
        @Column(name = "vod_pwd_url", nullable = false, length = 255)
        var vodPwdUrl: String? = null,

        @Basic
        @Column(name = "vod_pwd_play", nullable = false, length = 10)
        var vodPwdPlay: String? = null,

        @Basic
        @Column(name = "vod_pwd_play_url", nullable = false, length = 255)
        var vodPwdPlayUrl: String? = null,

        @Basic
        @Column(name = "vod_pwd_down", nullable = false, length = 10)
        var vodPwdDown: String? = null,

        @Basic
        @Column(name = "vod_pwd_down_url", nullable = false, length = 255)
        var vodPwdDownUrl: String? = null,


        @Basic
        @Column(name = "vod_play_from", nullable = false, length = 255)
        var vodPlayFrom: String? = null,

        @Basic
        @Column(name = "vod_play_server", nullable = false, length = 255)
        var vodPlayServer: String? = null,

        @Basic
        @Column(name = "vod_play_note", nullable = false, length = 255)
        var vodPlayNote: String? = null,


        @Basic
        @Column(name = "vod_down_from", nullable = false, length = 255)
        var vodDownFrom: String? = null,

        @Basic
        @Column(name = "vod_down_server", nullable = false, length = 255)
        var vodDownServer: String? = null,

        @Basic
        @Column(name = "vod_down_note", nullable = false, length = 255)
        var vodDownNote: String? = null,

        @Basic
        @Column(name = "vod_down_url", nullable = false, length = -1)
        var vodDownUrl: String? = null,

        @Basic
        @Column(name = "vod_plot", nullable = false)
        var vodPlot: Byte = 0,

        @Basic
        @Column(name = "vod_plot_name", nullable = false, length = -1)
        var vodPlotName: String? = null,

        @Basic
        @Column(name = "vod_plot_detail", nullable = false, length = -1)
        var vodPlotDetail: String? = null,
)

// 自定义反序列化器
class TimestampDeserializer : JsonDeserializer<Int>() {
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Int {
        val timeString = p.text // 获取 JSON 中的时间字符串
        return dateFormat.parse(timeString).toInstant().epochSecond.toInt()
    }
}
