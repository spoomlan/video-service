package org.nudt.common.model

import jakarta.persistence.*

@Entity
@Table(name = "version")
data class Version(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Int? = null,
    
    @Column(name = "update_msg", length = 500)
    var updateMsg: String? = null,
    
    @Column(name = "download_url")
    var downloadUrl: String? = null,
    
    @Column(name = "is_force")
    var isForce: Byte? = null,
    
    @Column(name = "version")
    var version: String? = null
)