package org.nudt.common.model.fetch

data class RespData<T>(
        val code: Int,
        val limit: String,
        val list: List<T>,
        val msg: String,
        val page: String,
        val pagecount: Int,
        val total: Int
)