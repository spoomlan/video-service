package org.nudt.common.model

data class ResponseData<T>(
    val code: Int,
    val msg: String,
    val data: T
) {
    companion object {
        fun <T> success(data: T): ResponseData<T> {
            return ResponseData(200, "success", data)
        }
    }
}