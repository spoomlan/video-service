package org.nudt.common.model

import jakarta.persistence.*

@Entity
@Table(name = "play_log")
data class PlayLog(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Int? = null,
    
    @Column(name = "video_id")
    var videoId: Int? = null,
    
    @Column(name = "user_id", nullable = false)
    var userId: Int? = null,
    
    @Column(name = "appid", nullable = false)
    var appid: String
) : BaseEntity()