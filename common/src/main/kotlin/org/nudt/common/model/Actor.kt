package org.nudt.common.model

import jakarta.persistence.*

@Entity
@Table(name = "actor")
data class Actor(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Int? = null,
    
    @Column(name = "type_id", nullable = false)
    var typeId: Int? = null,
    
    @Column(name = "actor_name", nullable = false)
    var actorName: String? = null,
    
    @Column(name = "actor_en", nullable = false)
    var actorEn: String? = null,
    
    @Column(name = "actor_sex", nullable = false)
    var actorSex: Char? = null,
    
    @Column(name = "actor_pic", nullable = false, length = 1024)
    var actorPic: String? = null,
    
    @Column(name = "actor_remarks", nullable = false, length = 100)
    var actorRemarks: String? = null,
    
    @Column(name = "actor_area", nullable = false, length = 20)
    var actorArea: String? = null,
    
    @Column(name = "actor_height", nullable = false, length = 10)
    var actorHeight: String? = null,
    
    @Column(name = "actor_weight", nullable = false, length = 10)
    var actorWeight: String? = null,
    
    @Column(name = "actor_birthday", nullable = false, length = 10)
    var actorBirthday: String? = null,
    
    @Column(name = "actor_school", nullable = false, length = 20)
    var actorSchool: String? = null,
    
    @Lob
    @Column(name = "actor_content", nullable = false)
    var actorContent: String? = null,
) : BaseEntity() 