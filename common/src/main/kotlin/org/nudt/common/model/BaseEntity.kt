package org.nudt.common.model

import jakarta.persistence.Column
import jakarta.persistence.MappedSuperclass
import java.time.LocalDateTime

@MappedSuperclass
open class BaseEntity {
    @Column(insertable = false)
    var createAt: LocalDateTime? = null

    @Column(insertable = false)
    var updateAt: LocalDateTime? = null
}