package org.nudt.common.model

import jakarta.persistence.*

@Entity
@Table(name = "admin")
data class Admin(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Int? = null,
    
    @Column(name = "username", nullable = false, length = 30)
    var username: String? = null,
    
    @Column(name = "password", nullable = false, length = 100)
    var password: String? = null,
    
    @Column(name = "avatar", length = 100)
    var avatar: String? = null,
) : BaseEntity()