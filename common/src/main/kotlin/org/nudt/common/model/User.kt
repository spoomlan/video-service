package org.nudt.common.model

import jakarta.persistence.*

@Entity
@Table(name = "user")
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Int? = null,
    
    @Column(name = "username", nullable = false, length = 50)
    var username: String,
    
    @Column(name = "nickname", length = 30)
    var nickname: String? = null,
    
    @Column(name = "appid", nullable = false, length = 50)
    var appid: String,
    
    @Column(name = "avatar", length = 100)
    var avatar: String? = null,
    
    @Column(name = "gender")
    var gender: Int? = null,
    
    @Column(name = "phone", length = 15)
    var phone: String? = null,
    
    @Column(name = "email", length = 50)
    var email: String? = null,
    
    @Column(name = "app_version", length = 20)
    var appVersion: String? = null,
    
    @Column(name = "os_version")
    var osVersion: Int? = null,
    
    @Column(name = "device_name", length = 50)
    var deviceName: String? = null,
    
    @Column(name = "enabled")
    var enabled: Byte? = null
) : BaseEntity()