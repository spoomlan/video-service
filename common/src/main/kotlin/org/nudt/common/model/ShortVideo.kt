package org.nudt.common.model

import jakarta.persistence.*

@Entity
@Table(name = "short_video")
data class ShortVideo(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Int? = null,
    
    @Column(name = "video_name")
    var videoName: String? = null,
    
    @Column(name = "owner")
    var owner: String? = null,
    
    @Column(name = "avatar")
    var avatar: String? = null,
    
    @Column(name = "play_url")
    var playUrl: String? = null
)