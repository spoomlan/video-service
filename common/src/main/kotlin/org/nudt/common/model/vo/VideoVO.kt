package org.nudt.common.model.vo

import org.nudt.common.model.Video
import java.math.BigDecimal

data class VideoVO(
    var id: Int?,
    var type: Int?,
    var videoName: String?,
    var pic: String?,
    var picThumb: String?,
    var score: BigDecimal?
) {
    companion object {
        fun of(video: Video): VideoVO {
            return VideoVO(video.vodId, video.typeId, video.vodName, video.vodPic, video.vodPicThumb, video.vodScore)
        }
    }
}