package org.nudt.common.repository;

import org.nudt.common.model.Actor
import org.springframework.data.jpa.repository.JpaRepository

interface ActorRepository : JpaRepository<Actor, Int> {
}