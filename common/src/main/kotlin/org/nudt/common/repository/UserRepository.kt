package org.nudt.common.repository

import org.nudt.common.model.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository : JpaRepository<User, Int> {
    fun findByAppid(appid: String): Optional<User>
    
}