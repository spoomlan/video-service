package org.nudt.common.repository;

import org.nudt.common.model.PlayLog
import org.springframework.data.jpa.repository.JpaRepository

interface PlayLogRepository : JpaRepository<PlayLog, Int> {
}