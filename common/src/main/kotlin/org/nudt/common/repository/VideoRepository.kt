package org.nudt.common.repository;

import org.nudt.common.model.Video
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query

interface VideoRepository : JpaRepository<Video, Int>, JpaSpecificationExecutor<Video> {

    fun getByTypeIdOrderByVodTime(type: Int, pageable: Pageable): Page<Video>

    @Query(
            """
        from Video where typeId = :type order by RAND()
        """
    )
    fun getVideosOrderByRand(type: Int, pageable: Pageable): List<Video>

    fun getByVodLevel(banner: Int): List<Video>

    fun getByVodNameContainsIgnoreCase(keyword: String): List<Video>

    fun getTopByVodName(vodName: String): Video?

    fun findByVodPicStartingWith(keyword: String, pageable: Pageable): List<Video>
}