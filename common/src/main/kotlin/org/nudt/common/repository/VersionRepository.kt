package org.nudt.common.repository;

import org.nudt.common.model.Version
import org.springframework.data.jpa.repository.JpaRepository

interface VersionRepository : JpaRepository<Version, Int> {
    
    fun findFirstByIdNotNullOrderByVersionDesc(): Version
}