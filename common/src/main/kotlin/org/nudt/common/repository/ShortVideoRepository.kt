package org.nudt.common.repository;

import org.nudt.common.model.ShortVideo
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface ShortVideoRepository : JpaRepository<ShortVideo, Int> {
    
    @Query(
        """
        from ShortVideo order by RAND()
        """
    )
    fun getShortVideosOrderByRand(pageable: Pageable): List<ShortVideo>
}