package org.nudt.common.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.OkHttpClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.TimeUnit


@Configuration
class SomeConfig {
    @Bean
    fun objectMapper(): ObjectMapper {
        val mapper = jacksonObjectMapper()
        // 配置ObjectMapper
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        // json中的下划线命名转换为波峰命名
        mapper.propertyNamingStrategy = PropertyNamingStrategies.SNAKE_CASE
        return mapper
    }

    @Bean
    fun httpClient(): OkHttpClient {
        val httpClient: OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
            .build()
        return httpClient
    }
}