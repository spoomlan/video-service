package org.nudt.common.config

import org.nudt.common.security.AppidAuthenticationFilter
import org.nudt.common.security.ErrorAuthenticationEntryPoint
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.SecurityFilterChain
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
class SecurityConfig(private val filter: AppidAuthenticationFilter) {
    
    @Bean
    fun securityFilterChain(httpSecurity: HttpSecurity): SecurityFilterChain {
        return httpSecurity
            .csrf { it.disable() }
            .cors {
                val configuration = CorsConfiguration()
                configuration.allowedOriginPatterns = listOf("http://localhost:*")
                configuration.allowedMethods = listOf("*")
                configuration.allowedHeaders = listOf("*")
                val source = UrlBasedCorsConfigurationSource()
                source.registerCorsConfiguration("/**", configuration)
                it.configurationSource(source)
            }
            .sessionManagement {
                it.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            }
            .authorizeHttpRequests {
//                it.requestMatchers("/v1/user/login", "/v1/admin/login", "/v1/admin/register").permitAll()
//                    .requestMatchers("/v1/admin/**").hasAuthority("sapi")
//                    .requestMatchers("/v1/**").permitAll()
                it.anyRequest().permitAll()
                //                    .requestMatchers("/vapi/**").hasAuthority("vapi")
            }
            .addFilter(filter)
            .exceptionHandling { handle -> handle.authenticationEntryPoint(ErrorAuthenticationEntryPoint()) }
            .build()
    }
}