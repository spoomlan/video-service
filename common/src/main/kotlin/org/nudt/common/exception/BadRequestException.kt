package org.nudt.common.exception

import org.springframework.http.HttpStatus

open class BadRequestException(message: String) : BaseException(HttpStatus.BAD_REQUEST, message)