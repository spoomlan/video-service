package org.nudt.common.exception

import org.springframework.http.HttpStatus

open class BaseException(val code: HttpStatus, message: String) : RuntimeException(message)