package org.nudt.common.security

import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component

@Component
class AppidAuthenticationProvider : AuthenticationProvider {
    override fun authenticate(authentication: Authentication): Authentication {
        val appid = authentication.principal as String
        return if (appid == "12345678")
            UsernamePasswordAuthenticationToken.authenticated(appid, appid, listOf(SimpleGrantedAuthority("sapi")))
        else
            UsernamePasswordAuthenticationToken.authenticated(appid, appid, listOf(SimpleGrantedAuthority("vapi")))
    }

    override fun supports(authentication: Class<*>): Boolean {
        return UsernamePasswordAuthenticationToken::class.java.isAssignableFrom(authentication)
    }

}