package org.nudt.common.security

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component

@Component
class AppidAuthenticationManager(private val provider: AppidAuthenticationProvider) : AuthenticationManager {
    override fun authenticate(authentication: Authentication): Authentication {
        return provider.authenticate(authentication)
    }
}