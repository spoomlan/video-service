package org.nudt.common.security

import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import java.time.LocalDateTime

class ErrorAuthenticationEntryPoint : AuthenticationEntryPoint {
    override fun commence(request: HttpServletRequest, response: HttpServletResponse, authException: AuthenticationException) {
        response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        
        val resp = GlobalExceptionHandler.ExceptionResponse(
            HttpStatus.UNAUTHORIZED.reasonPhrase,
            HttpStatus.UNAUTHORIZED,
            HttpStatus.UNAUTHORIZED.value(),
            request.requestURI
        )

//        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//        val serializer = LocalDateSerializer(formatter)
//        val objectMapper = Jackson2ObjectMapperBuilder().serializers(serializer).build<ObjectMapper>()
        
        //todo 使用json简化返回值，优化
        response.writer.print(
            "{\"message\":\"${HttpStatus.UNAUTHORIZED.reasonPhrase}\"," +
                    "\"error\":\"${authException.message}\"," +
                    "\"status\":\"401\"," +
                    "\"path\":\"${request.requestURI}\"," +
                    "\"timestamp\":\"${LocalDateTime.now()}\"}"
        )
    }
}