plugins {
    alias(libs.plugins.springboot)
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.kotlin.jpa)
}

dependencies {
    api(libs.spring.jpa)
    api(libs.spring.security)
    api(libs.spring.web)
    api(libs.jackson.kotlin)
    api(libs.kotlin.reflect)
    api(libs.sa.token)
    api(libs.okhttp)
}