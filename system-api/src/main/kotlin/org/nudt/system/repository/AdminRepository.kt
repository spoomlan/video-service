package org.nudt.system.repository

import org.nudt.common.model.Admin
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface AdminRepository : JpaRepository<Admin, Int> {

    fun findByUsername(username: String): Optional<Admin>
}