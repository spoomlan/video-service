package org.nudt.system.controller

import org.nudt.common.model.Admin
import org.nudt.common.model.ResponseData
import org.nudt.system.service.SysAdminService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/admin")
class SysAdminController(private val sysAdminService: SysAdminService) {
    
    @PostMapping("/login")
    fun login(@RequestParam username: String, @RequestParam password: String): ResponseData<Admin> {
        val admin = sysAdminService.login(username, password)
        return ResponseData.success(admin)
    }
    
    @PostMapping("/register")
    fun register(@RequestParam username: String, @RequestParam password: String): ResponseData<Admin> {
        val admin = sysAdminService.register(username, password)
        return ResponseData.success(admin)
    }
    
    @GetMapping("/info")
    fun info(): String {
        return "info"
    }
}