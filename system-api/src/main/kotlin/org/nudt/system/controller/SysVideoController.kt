package org.nudt.system.controller

import org.nudt.common.model.ResponseData
import org.nudt.common.model.Video
import org.nudt.system.service.SysVideoService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/videos")
class SysVideoController(private val sysVideoService: SysVideoService) {
    
    @PostMapping("/")
    fun saveVideo(@RequestBody video: Video): ResponseData<Video?> {
        return ResponseData.success(sysVideoService.saveNotUpdate(video))
    }
}