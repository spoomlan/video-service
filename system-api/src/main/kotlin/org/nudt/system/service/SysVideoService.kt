package org.nudt.system.service

import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.nudt.common.exception.BadRequestException
import org.nudt.common.model.Video
import org.nudt.common.repository.VideoRepository
import org.nudt.common.utils.FileUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.PageRequest
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream
import java.util.*


@Service
class SysVideoService(
    private val videoRepository: VideoRepository,
    @Value("\${file.pic-path}") private val picPath: String,
    private val objectMapper: ObjectMapper,
    private val httpClient: OkHttpClient
) {
    private val logger = LoggerFactory.getLogger(SysVideoService::class.java)

    fun saveNotUpdate(video: Video): Video? {
        video.vodName?.let {
            val videoInDb = videoRepository.getTopByVodName(it)
            if (videoInDb == null) {
                video.vodId = null
                return videoRepository.save(video)
            } else {
                return videoInDb
            }
        }
        return video
    }

    /**
     * 判断是否已经存在视频信息
     */
    fun exists(video: Video): Boolean {
        video.vodName?.let {
            val videoInDb = videoRepository.getTopByVodName(it)
            return videoInDb != null
        }
        return false
    }

    /**
     * 获取到还没同步照片的视频列表
     */
    fun findByVodPicStartingWith(keyword: String, page: PageRequest): List<Video> {
        val videoList = videoRepository.findByVodPicStartingWith(keyword, page)
        return videoList
    }

    /**
     * 下载图片
     * @param exists true代表已经存在视频信息
     */
    @Async
    fun download(exists: Boolean, video: Video) {
        val typeId = video.typeId
        val url = video.vodPic ?: ""

        if (!url.startsWith("http")) {
            throw BadRequestException("无法下载url: $url！")
        }

        logger.info(Thread.currentThread().name + "----开始下载 " + typeId + "---" + url)

        val request = Request.Builder()
            .url(url)
            .build()

        val fileExt = FileUtil.getExtensionName(url)
        val imageTypes = "jpg png jpeg webp"
        if (fileExt != null && !imageTypes.contains(fileExt)) {
            throw BadRequestException("文件格式错误！, 仅支持 $imageTypes 格式")
        }
        val fileName = "${UUID.randomUUID().toString().replace("-", "")}.$fileExt"
        val destPath = picPath + typeId + "\\" + fileName
        try {
            val outputFile = File(destPath).getCanonicalFile()
            // 检测是否存在目录
            if (!outputFile.getParentFile().exists()) {
                if (!outputFile.getParentFile().mkdirs()) {
                    logger.error("error url: $url")
                    throw BadRequestException("have no dir")
                }
            }

            val response: Response = httpClient.newCall(request).execute() // 执行请求

            if (response.isSuccessful) {
                // 将响应体写入文件
                response.body.let { body ->
                    FileOutputStream(outputFile).use { outputStream ->
                        outputStream.write(body.bytes())
                    }
                    logger.info("图片下载成功，保存路径: ${outputFile.absolutePath}")
                    val pic = "upload/vod/$typeId/$fileName"
                    video.vodPic = pic
                    // 如果是新的视频，则清空vodId，确保插入
                    if (!exists) {
                        video.vodId = null
                    }
                    //保存一下原始图片信息
                    video.vodPicThumb = url
                    videoRepository.save(video)
                }
            } else {
                logger.info("请求失败，状态码: ${response.code}")
            }
        } catch (e: Exception) {
            logger.info("下载失败: ${e.message}")
        }
    }
}