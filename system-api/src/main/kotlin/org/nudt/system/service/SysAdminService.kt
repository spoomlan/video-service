package org.nudt.system.service

import org.nudt.common.exception.BadRequestException
import org.nudt.common.model.Admin
import org.nudt.system.repository.AdminRepository
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class SysAdminService(private val repository: AdminRepository) {

    fun login(username: String, password: String): Admin {
        val admin = repository.findByUsername(username)
        if (admin.isEmpty) {
            throw BadRequestException("not authentication")
        } else {
            if (BCryptPasswordEncoder().matches(password, admin.get().password)) {
                return admin.get().copy(password = "")
            } else {
                throw BadRequestException("password wrong")
            }
        }
    }

    fun register(username: String, password: String): Admin {
        val admin = repository.findByUsername(username)
        if (admin.isPresent) {
            throw BadRequestException("username repeat")
        } else {
            val encryptedPassword = BCryptPasswordEncoder().encode(password)
            val newAdmin = Admin(username = username, password = encryptedPassword)
            return repository.save(newAdmin)
        }
    }
}